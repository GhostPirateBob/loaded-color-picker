module.exports = {
  publicPath: "/",
  devServer: {
    port: 3001,
    host: "0.0.0.0",
    https: false,
    // key: "/home/admin/conf/web/ssl.color.lcprojects.com.au.key",
    // cert: "/home/admin/conf/web/ssl.color.lcprojects.com.au.crt"
  },
  configureWebpack: {
    mode: "development"
  }
}
